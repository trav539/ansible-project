The purpose of this project was to create an ansible playbook that would create
docker machines with filebeat and metricbeat installed on them for an ELK stack.

To begin with I used Azure labs to create a main sercuity group that limited
access to my personal IP using SSH and HTTP requests only.  I then created an
ansible jump-box VM to set up the docker machines with SSH.  Three VMs were
then created behind the same firewall from the main security group.

In the same Azure resource group I then created a second virtual network with
one VM on it to contain the ELK stack behind its own ELK security group.  The
ELK security group also limited access to my IP and only allowed SSH and HTTP
requests, but also allowed a connection to the jump-box and all three VMs the
jump-box will set up later.

A basic docker container is then installed onto the jump-box to run the
ansible yml files.  Inside of this docker container there is a hosts file
that all the VMs need to be added to so that the container can connect to
the VMs and set them up properly.  The ansible.cfg file is also installed to
/etc/ansible and set up to recognize the system using the correct private
IPs for the network.  The two config files to filebeat and metricbeat also need
to be installed before running their yml file.  However both the filebeat and
metricbeat config files are instaled inside of /etc/ansible/files not
/etc/ansible.  Next the yml file to allow and create an
ELK stack on the controlled docker containers was created.


  file located inside /etc/ansible
  file name install-elk.yml
---
  - name: Config elk stack with Docker
    hosts: elk
    become: true
    tasks:
    - name: Use more memory
      sysctl:
        name: vm.max_map_count
        value: '262144'
        state: present
        reload: yes
    - name: docker.io
      apt:
        update_cache: yes
        name: docker.io  
        state: present
    - name: Install pip
      apt:
        name: python3-pip
        state: present
    - name: Install Docker python module
      pip:
        name: docker
        state: present
    - name: download and launch a docker web container
      docker_container
        name: Elk
        image: sebp/elk:740
        state: started
        restart_policy: always
        published_ports:
        - 5601:5601
        - 9200:9200
        - 5044:5044
    - name: Enable docker service
      systemd:
      name: docker
      enabled: yes

Another yml also needs to be created to install filebeat and metric beat onto
the docker containers.

  file also located inside of /etc/ansible
  file name filebeat-playbook.yml.org
---
  - name: Install Filebeat and Metricbeat
    hosts: webservers
    become: yes
    tasks:
    - name: Download filebeat package
      command: curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.4.0-amd64/deb
    - name: Install filebeat package
      command: dpkg -i filebeat-7.4.0-amd64.deb
    - name: Copy over filebeat configuration
      copy:
        src: /etc/ansible/files/filebeat-config.yml
        dest: /etc/filebeat/filebeat/yml
        mode: '0644'
    - name: Enable and configure system module
      command: filebeat modules enable system
    - name: Setup filebeat
      command: filebeat setup
    - name: Start filebeat service
      command: service filebeat start
    - name: Download metricbeat package
      command: curl -L -O https://artifacts/elastic.co/downloads/beats/metricbeat/metricbeat-7.4.0-amd64.deb
    - name: Install metricbeat package
      command: dpkg -i metricbeat-7.4.0-amd64.deb
    - name: Copy over metricbeat configuration
      copy:
        src: /etc/ansible/files/metricbeat-config.yml
        dest: /etc/metricbeat/metricbeat.yml
        mode: '0644'
    - name: Enable and configure system module
      command: metricbeat modules enable system
    - name: Setup metricbeat
      command: metricbeat setup
    - name: Start metricbeat service
      command: service metricbeat start